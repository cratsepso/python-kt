#!/usr/bin/python
#-*- coding: utf-8-*

# Autor: Chris Rätsepso, 2016

# Skriptimiskeeled kontrolltöö (Python)

""" Skripti eesmärgiks on lugeda sisendfailist IT Kolledži tudengite nimed ja nende põhjal 
 moodustada igaühele tema ITK kasutajanimi ja E-mail; seejärel genereeritakse igale tudengile token (20 tähemärki) 
 ning kirjutatakse väljundfaili vastavad read kujul: kasutajanimi, tudengi nimi, e-mail ja token. """

# Imporditakse skripti jaoks vajalikud moodulid 

import sys
import os
import re
import random 
import string

# Salvestatakse kasutaja poolt sisestatud parameetrite arv muutujasse

parameetrid = len(sys.argv)

#Kontrollin, kas kasutaja sisestas õige arvu parameetreid

if parameetrid != 3:
    print "Kasutamine: %s Sisendfail Väljundfail" % sys.argv[0]
    sys.exit()
    
    
# Salvestatakse kasutaja poolt sisestatud sisendfail ja väljundfail muutujatesse

sisendfail = sys.argv[1]
valjundfail = sys.argv[2]

# Kontrollitakse, kas sisendfail on olemas - kui ei ole, siis teatatakse sellest ning skirpt lõpetab töö

sisend_olemas = os.path.isfile(sisendfail)

if sisend_olemas == False:
    print "Sisendfaili pole olemas!"
    sys.exit()
    
# Kontrollitakse, kas väljundfail on olemas ning vajadusel luuakse

valjund_olemas = os.path.isfile(valjundfail)

if valjund_olemas == False:
    open(valjundfail, 'a')
    
sisend = open(sisendfail)
sisu = []

# Sisendfailist loetakse kõik read list-i (v.a tühjad read)

for rida in sisend.read().splitlines():
   read = re.split(r'[\t,\s]*', rida)
   if read != ['']:
       sisu.append(read)
sisend.close() 

# Eemaldatakse listist esimene element, kus asub ebavajalik esimene rida sisendfailist
sisu.pop(0)

"""Salvestatakse väljundfaili info objektina, objektist hangitakse failisuurus. 
Kui väljundfail ei ole tühi, siis küsitakse kasutaja käest, kas ta soovib seda ülekirjutada."""

valjundinfo = os.stat(valjundfail)
if valjundinfo.st_size != 0:
    print "Kas soovid väljundfaili üle kirjutada? (vasta jah või ei)"
    vastus = raw_input()
    while (vastus != "ei") and (vastus != "jah"):
          print "Kas soovid väljundfaili üle kirjutada? (vasta jah või ei)"
          vastus = raw_input()
    if vastus == "ei":
        sys.exit()
        
    
# Avatakse väljundfail kirjutamiseks    
f = open (valjundfail, 'w')

""" Listi elemendid käiakse läbi ükshaaval ,salvestatakse vajalikud andmed eraldi muutujatesse,
 moodustatakse kasutajanimi vastavalt eesnimele ja perenimele """
 
for element in sisu:
    eesnimi = element [1].lower()
    esitaht = eesnimi[0]
    perenimi = element [2].lower()
    kasutajanimi = esitaht + perenimi
    taisnimi=element[1] + " " + element[2]
    
    # Kontrollitakse, kas kasutajanimi on üle 8 tähemärgi ning vajadusel lõigatakse kasutajanimi lühemaks
    
    if len(kasutajanimi) > 8:
        kasutajanimi = kasutajanimi[0:8]
        
    # Genereeritakse token sobivate tähtede ja sümbolitega, pikkuseks on 20 tähemärki
    
    symbolid = ''.join((string.lowercase, string.uppercase, string.digits, "_"))
    token =''.join(random.choice(symbolid) for sym in range(20))
    
    # Kirjutatakse tudengite andmed reahaaval väljundfaili
    
    f.write (kasutajanimi + ", " + taisnimi + ", " + eesnimi + "." + perenimi + "@itcollege.ee" + ", " + token +'\n')
f.close()




    
      

    


